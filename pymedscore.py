#! /usr/bin/python3

# Ein Programm zur Berechnung verschiedener medizinischer Scores

import operator
from datetime import datetime

def question(q):
    while True:
        print(q)
        answer = input('[j]a / [n]ein: ')
        if answer == 'j' or answer == 'n':
            break
        print("Ungültige Eingabe")

    return answer == 'j'

def age():
    while True:
        try:
            answer = int(input('\nGeburtsjahr: '))
        except ValueError:
            print("Ungültige Eingabe")
            continue
        age = datetime.now().year - answer
        if age <= 120 and age >= 0:
            break
        print("Ungültige Eingabe")
    
    return age

def gender():
    while True:
        answer = input('\nGeschlecht\n[w]eiblich / [m]ännlich: ')
        if answer == 'w' or answer == 'm':
            break
        print("Ungültige Eingabe")
        
    return answer
    
def integ(q):
    while True:
        try:
            answer = int(round(float(input(q))))
        except ValueError:
            print("Ungültige Eingabe")
            continue
        break
        
    return answer
    
def flo(q):
    while True:
        try:
            answer = float(input(q))
        except ValueError:
            print("Ungültige Eingabe")
            continue
        break
        
    return answer

def score(lis):
    result = ('', 0, 0)

    for q in lis:
        if q[3] == 'yesno':
            if question(q[0]):
                result = tuple(map(operator.add, result, q[1]))

        if q[3] == 'age':
            answer = age()
            if answer >= 65:
                result = tuple(map(operator.add, result, q[1]))
            if answer >= 75:
                result = tuple(map(operator.add, result, q[2]))

        if q[3] == 'gender':
            if gender() == 'w':
                result = tuple(map(operator.add, result, q[1]))

    return result

def chadsbled():
    lis = [('''
Chronische Herinsuffizienz oder
Mitralstenose oder
Klinisch relevante Mitralinsuffizienz oder
Linksventrikuläre Ejektionsfraktion in der Echokardiogrphie von <=40%''', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nArterielle Hypertonie', ('', 1, 1), ('', 0, 0), 'yesno'),
           (0, ('', 1, 1), ('', 1, 0), 'age'),
           ('\nDiabetes mellitus', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nSchlaganfall', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nIschämischer Schlaganfall oder\nTIA oder\nThromboembolie', ('', 2, 0), ('', 0, 0), 'yesno'),
           ('\nKHK oder\nPeriphere arterielle Verschlusskrankheit oder\nAortenplaque', ('', 1, 0), ('', 0, 0), 'yesno'),
           (0, ('w', 1 ,0), ('', 0, 0), 'gender'),
           ('\nAbnorme Funktion der Nieren', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nAbnorme Funktion der Leber', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nBlutungsneigung', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nLabile INR-Werte (unter Therapie mit Vitamin-K-Antagonist)', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nEinnahme von Thrombozytenaggregationshemmern oder NSAR', ('', 0, 1), ('', 0, 0), 'yesno'),
           ('\nAlkoholabusus', ('', 0, 1), ('', 0, 0), 'yesno')]

    result = score(lis)

    print('\nCHA2DS2-VASc-Score:', result[1], 'Punkte.')

    if result[1] == 0:
        print('Schlaganfallrisiko ohne Antikoagulation <1%/Jahr.')
    if result[1] == 1:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 1%/Jahr.')
    if result[1] == 2:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 2%/Jahr.')
    if result[1] == 3:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 3%/Jahr.')
    if result[1] == 4:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 4%/Jahr.')
    if result[1] == 5:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 7%/Jahr.')
    if result[1] == 6:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 10%/Jahr.')
    if result[1] == 7:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 10%/Jahr.')
    if result[1] == 8:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 13%/Jahr.')
    if result[1] == 9:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 15%/Jahr.')

    if result[0] == '' and result[1] == 0:
        print('Keine Therapie.')
    if result[0] == 'w' and result[1] == 1:
        print('Keine Therapie.')
    if result[0] == '' and result[1] == 1:
        print('Antikoagulation nach individueller Nutzen-Risiko-Abwägung.')
    if result[0] == 'w' and result[1] == 2:
        print('Antikoagulation nach individueller Nutzen-Risiko-Abwägung.')
    if result[0] == '' and result[1] >= 2:
        print('Antikoagulation.')
    if result[0] == 'w' and result[1] >= 3:
        print('Antikoagulation.')

    print('\nHAS-BLED-Score:', result[2], 'Punkte.')

    if result[2] <= 2:
        print('Niedriges bis moderates Blutungsrisiko.\nI.d.R. besteht keine Kontraindikation für eine Antikoagulation.')
    if result[2] > 2:
        print('''Hohes Blutungsrisiko.
Die Intensität der Antikoagulation muss abgewogen werden; optimierbare Risikofaktoren sollten behandelt bzw. in ihrer Behandlung optimiert werden.
Bei der Auswahl eines geeigneten Antikoagulans sollten ggf. zusätzlich vorliegende Risikofaktoren bzgl. medikamentöser Unverträglichkeiten und Interaktionen berücksichtigt werden.
Ein interventioneller Vorhofohrverschluss als Alternative zur Antikoagulation kann in Erwägung gezogen werden.''')

def lwells():
    lis = [('\nSchwellung eines Beins\nSchmerzen bei der Palpation der tiefliegenden Venen', ('', 3, 0), ('', 0, 0), 'yesno'),
           ('\nLungenembolie wahrscheinlicher als andere Diagnose', ('', 3, 0), ('', 0, 0), 'yesno'),
           ('\nFrühere Lungenembolie/TVT', ('', 1.5, 0), ('', 0, 0), 'yesno'),
           ('\nTachykardie (Herzfrequenz >100/min)', ('', 1.5, 0), ('', 0, 0), 'yesno'),
           ('\nOperation oder Immobilisierung innerhalb des letzten Monats', ('', 1.5, 0), ('', 0, 0), 'yesno'),
           ('\nHämoptysen', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nMalignom (unter Therapie, Palliativtherapie oder Diagnose jünger als 6 Monate)', ('', 1, 0), ('', 0, 0), 'yesno')]

    result = score(lis)

    print('\nWells-Score:', result[1], 'Punkte.')

    if result[1] <= 4:
        print('Lungenembolie unwahrscheinlich.')
    if result[1] > 4:
        print('Lungenembolie wahrscheinlich -> Angio-CT.')

    return result[1]

def twells():
    lis = [('\nAktive Krebserkrankung', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nFrühere, dokumentierte TVT', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nLähmung oder\nKürzliche Immobilisation der Beine', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nBettruhe (>3 Tage) oder\nGroße chirurgische Operation (<12 Wochen)', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nSchmerz oder Verhärtung entlang der tiefen Venen', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nSchwellung des gesamten Beines', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nUmfang eines Unterschenkels >3 cm größer als Gegenseite', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nEindrückbares Ödem am symptomatischen Bein', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nSichtbare Kollateralvenen', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nAlternative Diagnose mindestens ebenso wahrscheinlich wie TVT', ('', -2, 0), ('', 0, 0), 'yesno')]

    result = score(lis)

    print('\nWells-Score:', result[1], 'Punkte.')

    if result[1] < 2:
        print('''Wahrscheinlichkeit für TVT nicht hoch -> D-Dimere bestimmen.
D-Dimere normwertig -> TVT ausgeschlossen.
D-Dimere erhöht -> (Farbduplex-) Kompressionssonographie.
(Farbduplex-) Kompressionssonographie negativ -> TVT ausgeschlossen.
(Farbduplex-) Kompressionssonographie positiv -> Therapie einleiten.''')
    if result[1] >= 2:
        print('''Wahrscheinlichkeit für TVT hoch -> (Farbduplex-) Kompressionssonographie.
(Farbduplex-) Kompressionssonographie negativ -> TVT ausgeschlossen.
(Farbduplex-) Kompressionssonographie positiv -> Therapie einleiten.''')

def lae():
    while True:

        if question('\nRR syst. >90mmHg'):
            if question('\nRR-Abfall >40mmHg (>15min)'):
                if question('\nPat. stabil genug für CT'):
                    if question('\nPulmonale Embolie'):
                        print('\nVorliegen einer LE -> Therapie einleiten.')
                        break
                    else:
                        print('\nLE ausgeschlossen -> weitere Diagnostik.')
                        break
                else:
                    if question('\nEchokardiographie: Rechtsventrikuläre Dysfunktion'):
                        print('\nVorliegen einer LE -> Therapie einleiten.')
                        break
                    else:
                        print('\nFulminante LE ausgeschlossen -> weitere Diagnostik.')
                        break
            else:
                score = lwells(lis2)
                if score <= 4:
                    if question('\nD-Dimere normwertig'):
                        print('\nLE ausgeschlossen -> weitere Diagnostik.')
                        break
                    else:
                        if question('\nAngio-CT: Pulmonale Embolie'):
                            print('\nVorliegen einer LE -> Therapie einleiten.')
                            break
                        else:
                            print('\nLE ausgeschlossen -> weitere Diagnostik.')
                            break
                if score > 4:
                    if question('\nAngio-CT: Pulmonale Embolie'):
                        print('\nVorliegen einer LE -> Therapie einleiten.')
                        break
                    else:
                        print('\nLE ausgeschlossen -> weitere Diagnostik.')
                        break
        else:
            if question('\nPat. stabil genug für CT'):
                if question('\nPulmonale Embolie'):
                    print('\nVorliegen einer LE -> Therapie einleiten.')
                    break
                else:
                    print('\nLE ausgeschlossen -> weitere Diagnostik.')
                    break
            else:
                if question('\nEchokardiographie: Rechtsventrikuläre Dysfunktion'):
                    print('\nVorliegen einer LE -> Therapie einleiten.')
                    break
                else:
                    print('\nFulminante LE ausgeschlossen -> weitere Diagnostik.')
                    break

def gcs():
    score = 0

    print('''
Augen öffnen?

keine Reaktion [1]
bei Schmerzreiz [2]
bei Aufforderung [3]
spontan [4]''')

    answer = input()
    score += int(answer)

    print('''
Verbale Reaktion?

keine Reaktion [1]
unverständliche Laute [2]
unzusammenhängende Wörter [3]
desorientiert, aber konversationsfähig [4]
orientiert und konversationsfähig [5]''')

    answer = input()
    score += int(answer)

    print('''
Motorische Reaktion?

keine Reaktion [1]
reagiert mit Strecksynergismen auf Schmerzreiz [2]
reagiert mit Beugesynergismen auf Schmerzreiz [3]
reagiert ungezielt auf Schmerzreize [4]
reagiert gezielt auf Schmerzreize [5]
befolgt Aufforderungen [6]''')

    answer = input()
    score += int(answer)

    print('\nGCS-Score:', score, 'Punkte.')

    return score

def sofa():
    score = 0

    answer = hw()

    if answer < 100:
        score += 4
    elif answer < 200:
        score += 3
    elif answer < 300:
        score += 2
    elif answer < 400:
        score += 1

    answer = float(input('\nKreatinin (mg/dL): '))

    if answer >= 5:
        score += 4
    elif answer >= 3.5:
        score += 3
    elif answer >= 2:
        score += 2
    elif answer >= 1.2:
        score += 1

    answer = float(input('\nBilirubin (mg/dL): '))

    if answer >= 12:
        score += 4
    elif answer >= 6:
        score += 3
    elif answer >= 2:
        score += 2
    elif answer >= 1.2:
        score += 1

    if question('\nKatecholamine?'):
        answer = input('''
Katecholamindosis?

hoch: Noradrenalin >0,1µg/kgKG/min (4,2mL/h bei 5mg auf 50ml bei 70kgKG) [h]
mittel: Noradrenalin <=0,1µg/kgKG/min [m]
niedrig: z.B. Dobutamin >=1h [n]
''')

        if answer == 'h':
            score += 4
        if answer == 'm':
            score += 3
        if answer == 'n':
            score += 2

    else:
        sanswer = int(input('\nsystolischer Blutdruck (mmHg): '))
        danswer = int(input('\ndiastolischer Blutdruck (mmHg): '))

        answer = danswer + ((sanswer - danswer) / 2)

        if answer < 70:
            score += 1

    answer = int(input('\nThrombozyten (1.000/mm^3): '))

    if answer < 20:
        score += 4
    elif answer < 50:
        score += 3
    elif answer < 100:
        score += 2
    elif answer < 150:
        score += 1

    answer = gcs()

    if answer < 6:
        score += 4
    elif answer < 10:
        score += 3
    elif answer < 13:
        score += 2
    elif answer < 15:
        score += 1

    print('\nSOFA-Score:', score, 'Punkte.\nAnstieg >= 2 Punkte spricht für eine Organdysfunktion (bspw. bei Sepsis).\nIst der Score zuvor nicht erfasst worden, gilt als Vergleichswert 0.')

def qsofa():
    lis = [('\nAtemfrequenz >=22/min ?', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nSystolischer Blutdruck <100mmHg ?', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nVigilanzminderung oder veränderter mentaler Status?', ('', 1, 0), ('', 0, 0), 'yesno')]

    result = score(lis)

    print('\nqSOFA-Score:', result[1], 'Punkte.')

    if result[1] <= 1:
        print('Septische Organdysfunktion bzw. schlechte Prognose unwahrscheinlich.')
    if result[1] == 2:
        print('3-fach erhöhte Sterblichkeit.\nÜberwachung und Eskalation von Diagnostik und Therapie bezüglich Sepsis.')
    if result[1] == 3:
        print('14-fach erhöhte Sterblichkeit.\nÜberwachung und Eskalation von Diagnostik und Therapie bezüglich Sepsis.')

def perf():
    dose = float(input('\nDosis (mg/kgKG/min): '))
    amount = int(input('\nMenge Medikament (mg): '))
    volume = int(input('\nPerfusorspritzenvolumen (mL): '))
    weight = int(input('\nKörpergewicht Patient (kg): '))
    rate = input('\nLaufrate in mL/h [h] oder mL/min [m]: ')

    if rate == 'm':
        result = dose * ((volume * weight) / amount)
        print('\nLaufrate:', round(result, 2), 'mL/min')

    if rate == 'h':
        result = dose * ((volume * weight * 60) / amount)
        print('\nLaufrate:', round(result, 2), 'mL/h')

def copd():
    print('\nDer Schweregrad der COPD wird nicht während der akuten Exazerbation bestimmt!')
    answer = int(input('\nTiffeneau-Index FEV1/VC (% vom Soll): '))
    if answer >= 70:
        print('\nCOPD unwahrscheinlich')
    else:
        answer = int(input('\nFEV1 (% vom Soll): '))
        if answer < 30:
            print('\nCOPD Grad IV')
        elif answer < 50:
            oanswer = int(input('\npO2 bei Raumluft (mmHg): '))
            canswer = int(input('\npCO2 bei Raumluft (mmHg): '))
            if oanswer < 60 or canswer > 50:
                print('\nCOPD Grad IV')
            else:
                print('\nCOPD Grad III')
        elif answer < 80:
            print('\nCOPD Grad II')
        else:
            print('\nCOPD Grad I')

def gfr():
    weight = int(input('\nKörpergewicht (kg): '))
    crea = float(input('\nSerum-Kreatinin (mg/dL): '))

    result = (140 - age()) * (weight / (72 * crea))

    if gender() == 'w':
        result *= 0.85

    print('\neGFR nach Cockcroft-Gault-Formel =', round(result), 'mL/min')

    return result

def nkf():

    egfr = integ('\neGFR (mL/min): ')

    if egfr < 15:
        print('\nNKF-Stadium 5')
    elif egfr < 30:
        print('\nNKF-Stadium 4')
    elif egfr < 45:
        print('\nNKF-Stadium 3b')
    elif egfr < 60:
        print('\nNKF-Stadium 3a')
    else:
        if question('\nHinweise auf bestehenden Nierenschaden in Blut- oder Urinwerten oder Bildgebung'):
            if egfr < 90:
                print('\nNKF-Stadium 2')
            else:
                print('\nNKF-Stadium 1')
        else:
            print('\nChronische Niereninsuffizienz unwahrscheinlich.')

def kdigo():
    akrea = float(input('\nAktuelles Serum-Kreatinin (mg/dL): '))
    vkrea = float(input('\nVorwert <7 Tage Serum-Kreatinin (mg/dL)\nUnbekannt = 0: '))

    if akrea >= 4 and vkrea < 4:
        kresult = 3
    elif vkrea > 0 and akrea >= vkrea * 2.99:
        kresult = 3
    elif question('\nBeginn einer Nierenersatztherapie'):
        kresult = 3
    elif age() < 18:
        if int(input('\neGFR (mL/min): ')) < 35:
            kresult = 3
    elif vkrea > 0 and akrea >= vkrea * 1.99:
        kresult = 2
    elif vkrea > 0 and akrea >= vkrea * 1.49:
        kresult = 1
    else:
        tkrea = float(input('\nVorwert <48 Stunden Serum-Kreatinin (mg/dL)\nUnbekannt = 0: '))
        if tkrea > 0 and akrea >= tkrea + 0.29:
            kresult = 1
        else:
            kresult = 0

    if kresult < 3 and question('\nUrinbilanzierung'):
        weight = int(input('\nKörpergewicht (kg): '))
        qexcr = int(input('\nUrin Ausscheidung in den letzten 6 Stunden (mL): '))
        hexcr = int(input('\nUrin Ausscheidung in den letzten 12 Stunden (mL): '))
        dexcr = int(input('\nUrin Ausscheidung in den letzten 24 Stunden (mL): '))

        if hexcr < 50:
            uresult = 3
        elif dexcr < 7.2 * weight:
            uresult = 3
        elif hexcr < 6 * weight:
            uresult = 2
        elif qexcr < 3 * weight:
            uresult = 1
        else:
            uresult = 0
    else:
        uresult = 0

    if kresult == uresult and kresult == 0:
        print('\nAkute Niereninsuffizienz unwahrscheinlich oder zu wenig Messwerte.')
    elif kresult >= uresult:
        print ('\nKDIGO-Stadium', kresult)
    elif uresult > kresult:
        print ('\nKDIGO-Stadium', uresult)

def bmi():
    weight = int(input('\nKörpergewicht (kg): '))
    height = int(input('\nKörpergröße (cm): '))

    result = round((weight / ((height / 100) ** 2)), 1)

    print('\nBMI =', result)

    if result < 18.5:
        print('Untergewicht')
    elif result < 25:
        print('Normalgewicht')
    elif result < 30:
        print('Präadipositas')
    elif result < 35:
        print('Adipositas Grad I')
    elif result < 40:
        print('Adipositas Grad II')
    else:
        print('Adipositas Grad III')

def hw():
    panswer = int(input('\nPaO2 (mmHg): '))
    fanswer = int(input('\nFiO2 (%): '))

    return (panswer / (fanswer / 100))
    
def chad():
    lis = [('''
Chronische Herinsuffizienz oder
Mitralstenose oder
Klinisch relevante Mitralinsuffizienz oder
Linksventrikuläre Ejektionsfraktion in der Echokardiogrphie von <=40%''', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nArterielle Hypertonie', ('', 1, 1), ('', 0, 0), 'yesno'),
           (0, ('', 1, 1), ('', 1, 0), 'age'),
           ('\nDiabetes mellitus', ('', 1, 0), ('', 0, 0), 'yesno'),
           ('\nIschämischer Schlaganfall oder\nTIA oder\nThromboembolie', ('', 2, 0), ('', 0, 0), 'yesno'),
           ('\nKHK oder\nPeriphere arterielle Verschlusskrankheit oder\nAortenplaque', ('', 1, 0), ('', 0, 0), 'yesno'),
           (0, ('w', 1 ,0), ('', 0, 0), 'gender')]

    result = score(lis)

    print('\nCHA2DS2-VASc-Score:', result[1], 'Punkte.')

    if result[1] == 0:
        print('Schlaganfallrisiko ohne Antikoagulation <1%/Jahr.')
    if result[1] == 1:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 1%/Jahr.')
    if result[1] == 2:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 2%/Jahr.')
    if result[1] == 3:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 3%/Jahr.')
    if result[1] == 4:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 4%/Jahr.')
    if result[1] == 5:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 7%/Jahr.')
    if result[1] == 6:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 10%/Jahr.')
    if result[1] == 7:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 10%/Jahr.')
    if result[1] == 8:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 13%/Jahr.')
    if result[1] == 9:
        print('Schlaganfallrisiko ohne Antikoagulation ca. 15%/Jahr.')

    if result[0] == '' and result[1] == 0:
        print('Keine Therapie.')
    if result[0] == 'w' and result[1] == 1:
        print('Keine Therapie.')
    if result[0] == '' and result[1] == 1:
        print('Antikoagulation nach individueller Nutzen-Risiko-Abwägung.')
    if result[0] == 'w' and result[1] == 2:
        print('Antikoagulation nach individueller Nutzen-Risiko-Abwägung.')
    if result[0] == '' and result[1] >= 2:
        print('Antikoagulation.')
    if result[0] == 'w' and result[1] >= 3:
        print('Antikoagulation.')
        
def qtc():
    hf = flo('\nHerzfrequenz (1/min): ')
    qt = flo('\nQT-Zeit (ms): ')
    
    qtc = round(qt / ((60.0 / hf) ** (0.5)))
    
    frac = round((float(qtc) / 390.0) * 100)
    
    print('\nQTc =', qtc, 'ms (' + str(frac) + '%)')

print('''
KDIGO-Stadium der akuten Niereninsuffizienz [k]
Alter berechnen [a]
BMI [b]
CHA2DS2-VASC-Score und HAS-BLED-Score [c]
CHA2DS2-VASC-Score [ch]
COPD-Schweregrad [co]
GCS-Score [g]
eGFR nach Cockroft-Gault-Formel [gf]
Horovitz-Quotient [h]
LAE-Diagnostik-Algorithmus [l]
NKF-Stadium der chronischen Niereninsuffizienz [n]
Perfusorlaufrate [p]
qSOFA-Score [q]
QTc nach Bazett [qt]
SOFA-Score [s]
Wells-Score bei V.a. LAE [w]
Wells-Score bei V.a. TVT [t]''')
answer = input()
if answer == 'c':
    chadsbled()
if answer == 'l':
    lae()
if answer == 'w':
    lwells()
if answer == 't':
    twells()
if answer == 'g':
    gcs()
if answer == 's':
    sofa()
if answer == 'q':
    qsofa()
if answer == 'p':
    perf()
if answer == 'co':
    copd()
if answer == 'a':
    print('\nAlter:', age())
if answer == 'gf':
    gfr()
if answer == 'n':
    nkf()
if answer == 'k':
    kdigo()
if answer == 'b':
    bmi()
if answer == 'h':
    print('\nHorovitz-Quotient:', round(hw()))
if answer == 'ch':
    chad()
if answer == 'qt':
    qtc()